use raster::{editor, ResizeMode};
use std::convert::TryInto;
use std::{fs, u8};

const BINARY: bool = true;

fn main() {
	let mut image = raster::open("assets/stone_plus_ergaster.jpg").unwrap();
	editor::resize(&mut image, 100, 100, ResizeMode::ExactWidth).unwrap();
	let mut ascii = String::new();

	let asc_eq = if BINARY {
		vec!["  ", "11", "00"]
	} else {
		vec![
			"  ", "..", ",,", "--", "++", "**", "==", "oo", "aa", "##", "@@",
		]
	};

	let steps = u8::MAX as usize / asc_eq.len();

	for row in 0..image.height {
		for col in 0..image.width {
			let pixel = image.get_pixel(col, row).unwrap();
			// convert rgb color into Grey scale
			let grey_value: u8 = ((pixel.r as u32 + pixel.b as u32 + pixel.g as u32) / 3)
				.try_into()
				.unwrap();

			let index = grey_value / (steps + 1) as u8;

			ascii.push_str(asc_eq[index as usize]);
		}
		ascii.push_str("\n")
	}

	fs::write("ascii-stone-plus-ergaster.txt", ascii).unwrap();
}
